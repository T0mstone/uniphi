# UniΦ

This crate is a small helper for writing variable unification code without ids/lookup tables.

## What is unification?

Unification is a process for making two variables identical, while combining their values in a predefined way.
- Important for combining values is that, when given two of the same value, that same value is returned again.

Variables, in this context, are *references to values* (not necessarily `&`-references, but references in a broader sense).

Unifying two variables then means that you combine their values to a new value (which can also just be one of the two initial values)
and then let both of the variables reference that new value.
- This means that any subsequent changes to that new value will affect *both* of the variables, i.e. they are identical.

Examples of combining values:
- A basic example of this would be using natural numbers as values and `gcd` as the combining operation (since `gcd(x, x) == x`)
- The real-world usecase this was made for is for solving equations, specifically in type systems of programming languages,
where types can be partially known and we want to take two partial types that we have determined to be the same type
and combine (unify) them into one partial type that combines all information the two parts had separately.

## The lookup table approach

By far the most common approach to this problem is storing values in a lookup table (usually a [`HashMap`](std::collections::HashMap) or a [`Vec`])
and using their keys (usually some form of numeric ids) as the variables.

This approach is easy to work with, but it is not fail-safe:
Since the lookup table is detached from the keys, you have to handle the possibility of an invalid key,
which is not really ideal.

## How this crate works

UniΦ works by using [`Rc`]s as variables, so variables are actually pointers to their values.
This means that there can never be a variable without a value and thus the `variable->value` lookup is infallible (see [`Variable::value`]).
Also, this means that once a value has no more variables pointing to it, it will ✨automagically✨(🤮) be dropped.

This has some drawbacks, however:
1. Each value has its own allocation, as opposed to them all being stored in one big allocation.
This may affect performance or memory overhead.
2. Due to use of [`Rc`], some sort of interior mutability is also needed. This crate opts for [`qcell`],
which provides types similar to [`RefCell`](std::cell::RefCell) that are borrow-checked at compile-time.
The encouraged pattern here is to use the same owner for all variables, but that also has a drawback:
Nested variables become impossible to access without a workaround (see [`nested`]).

[`Vec`]: ::std::vec::Vec
[`Rc`]: ::std::rc::Rc
[`Variable::value`]: crate::Variable::value
[`qcell`]: ::qcell
[`nested`]: crate::nested
