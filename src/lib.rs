#![doc = include_str!("../README.md")]
#![warn(missing_docs)]

use std::marker::PhantomData;
use std::rc::Rc;

pub use ::qcell;

use crate::cell::{TransparentXCell, UniquelyOwnedXCell, XCell, XCellOwner};

/// An abstraction for the different types in [`qcell`]
pub mod cell;

/// Shorthand for `Variable<QCell<T>>`
pub type QVariable<T> = Variable<qcell::QCell<T>>;
/// Shorthand for `Variable<TCell<Q, T>>`
pub type TVariable<Q, T> = Variable<qcell::TCell<Q, T>>;
/// Shorthand for `Variable<TLCell<Q, T>>`
pub type TLVariable<Q, T> = Variable<qcell::TLCell<Q, T>>;
/// Shorthand for `Variable<LCell<'id, T>>`
pub type LVariable<'id, T> = Variable<qcell::LCell<'id, T>>;

/// A variable that points to a value
#[repr(transparent)]
pub struct Variable<C: XCell> {
	inner: Rc<C>,
}

impl<C: XCell> Clone for Variable<C> {
	fn clone(&self) -> Self {
		Self {
			inner: Rc::clone(&self.inner),
		}
	}
}

impl<C: XCell> Variable<C> {
	/// Construct a variable from an already existing cell.
	pub fn from_cell(cell: C) -> Self {
		Self {
			inner: Rc::new(cell),
		}
	}

	/// Construct a variable, using the specified owner to make a new cell.
	pub fn new_with_owner<O>(owner: &C::Owner, value: C::Value) -> Self
	where
		C::Value: Sized,
	{
		Self::from_cell(owner.cell(value))
	}

	/// Construct a variable, creating a new cell whose owner is determined by its type.
	pub fn new(value: C::Value) -> Self
	where
		C::Value: Sized,
		C: UniquelyOwnedXCell,
	{
		Self::from_cell(C::new(value))
	}

	/// Obtain a shared reference to the contained cell.
	pub fn as_cell(&self) -> &C {
		&self.inner
	}

	/// Borrow the pointed-to value
	pub fn value<'a>(&'a self, owner: &'a C::Owner) -> &'a C::Value {
		owner.ro(&*self.inner)
	}

	/// Borrow the pointed-to value mutably
	pub fn value_mut<'a>(&'a self, owner: &'a mut C::Owner) -> &'a mut C::Value {
		owner.rw(&*self.inner)
	}

	/// Get the value by value if this is the only variable pointing to that value
	pub fn into_unshared_value(self) -> Option<C::Value>
	where
		C::Value: Sized,
	{
		Rc::into_inner(self.inner).map(C::into_inner)
	}

	/// Get a mutable reference to the value without needing to use an owner if this is the only variable pointing to that value
	pub fn unshared_value_mut(&mut self) -> Option<&mut C::Value> {
		Rc::get_mut(&mut self.inner).map(C::get_mut)
	}

	/// Get a pointer to the underlying value
	pub fn as_ptr(&self) -> *const C::Value
	where
		C: TransparentXCell,
	{
		let cell_ptr = Rc::as_ptr(&self.inner);
		C::ptr_to_inner(cell_ptr)
	}

	/// Determine if `self` and `other` are pointing to the same value
	pub fn is_identical(&self, other: &Self) -> bool {
		Rc::ptr_eq(&self.inner, &other.inner)
	}

	/// The main function for unification.
	///
	/// Use `unify_values` to combine the values of `self` and `rhs`
	/// and set both `self` and `rhs` to point to that.
	pub fn make_identical<E, N>(
		&mut self,
		rhs: &mut Self,
		owner: &mut C::Owner,
		unify_values: impl FnOnce(&C::Owner, &C::Value, &C::Value) -> Result<ChooseOrNew<N>, E>,
	) -> Result<(), E>
	where
		C::Value: Sized,
		N: construct_new::ConstructNew<C::Value>,
	{
		if self.is_identical(rhs) {
			return Ok(());
		}

		match unify_values(owner, self.value(owner), rhs.value(owner))? {
			ChooseOrNew::Left => *rhs = self.clone(),
			ChooseOrNew::Right => *self = rhs.clone(),
			ChooseOrNew::New(n) => {
				let (lval, rval) = owner.rw2(&*self.inner, &*rhs.inner);

				let new_val = n.construct_new(lval, rval);
				*lval = new_val;
				*rhs = self.clone();
			}
		}

		Ok(())
	}

	/// Use a nested [`Variable`].
	///
	/// For more info, see the docs of [`nested`].
	pub fn use_nested<'a, F, CB, I>(
		&'a self,
		owner: &'a mut C::Owner,
		get_inner: F,
		cb: CB,
	) -> nested::UseNested<'a, C, F, CB, I> {
		nested::UseNested {
			var: self,
			owner,
			get_inner,
			cb,
			_marker: PhantomData,
		}
	}
}

/// A type encoding a lazy combination of two values
///
/// Having the explicit option of choosing one of the pre-existing values
/// reduces the amount of computation needed in that case.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum ChooseOrNew<N> {
	/// Choose the left value
	Left,
	/// Choose the right value
	Right,
	/// Something from which a new (combined) value can be made
	///
	/// Usually a function or an already pre-made value, see also [`ConstructNew`](construct_new::ConstructNew)
	New(N),
}

impl<N> ChooseOrNew<N> {
	/// Swaps `Left` and `Right`
	pub fn flip(self) -> Self {
		match self {
			Self::Left => Self::Right,
			Self::Right => Self::Left,
			Self::New(n) => Self::New(n),
		}
	}

	/// If `self` is `New(x)`, change `x` to something else; otherwise, leave `self` alone.
	pub fn map_new<M>(self, map: impl FnOnce(N) -> M) -> ChooseOrNew<M> {
		use ChooseOrNew::*;
		match self {
			Left => Left,
			Right => Right,
			New(x) => New(map(x)),
		}
	}
}

/// A module containing [`ConstructNew`](construct_new::ConstructNew) and implementors
pub mod construct_new {
	/// A trait designating a type that knows a way to combine two values
	pub trait ConstructNew<T> {
		#[allow(missing_docs)]
		fn construct_new(self, lhs: &mut T, rhs: &mut T) -> T;
	}

	/// Combine the two values using the given function.
	pub struct WithFn<F>(pub F);

	impl<T, F> ConstructNew<T> for WithFn<F>
	where
		F: FnOnce(&mut T, &mut T) -> T,
	{
		fn construct_new(self, lhs: &mut T, rhs: &mut T) -> T {
			self.0(lhs, rhs)
		}
	}

	/// Ignore the two values and call the given function to get the result.
	pub struct WithFn0<F>(pub F);

	impl<T, F> ConstructNew<T> for WithFn0<F>
	where
		F: FnOnce() -> T,
	{
		fn construct_new(self, _: &mut T, _: &mut T) -> T {
			self.0()
		}
	}

	/// Ignore the two values and just use `self.0` as the result.
	pub struct Constant<T>(pub T);

	impl<T> ConstructNew<T> for Constant<T> {
		fn construct_new(self, _: &mut T, _: &mut T) -> T {
			self.0
		}
	}
}

/// Workarounds for accessing nested variables
///
/// ## The problem
///
/// When you have a `Variable<XCell<MyStruct>>` and `MyStruct` has a `Variable<XCell<Foo>>` field you want to modify,
/// you need to borrow the owner mutably to access the value of type `MyStruct`, at which point you would need to borrow it mutably again
/// in order to modify the inner value of type `Foo`, while also having mutable access to the owner.
///
/// As an analogy, think of two safes that use the same key (which only exists once) and where one is inside the other.
/// To open the outer safe, you need the key, but to open the inner safe, you also need the key.
/// (This analogy has many flaws, but it illustrates why this is impossible to resolve without a workaround)
///
/// ## The solution(s)
///
/// This module presents several workarounds for the issue as methods of [`UseNested`].
/// You use them by first calling [`Variable::use_nested`], which returns a [`UseNested`],
/// and then calling one of the methods on that as a way of specifying which workaround you want to use.
///
/// [`UseNested`]: nested::UseNested
pub mod nested {
	use std::marker::PhantomData;
	use std::rc::Rc;

	use crate::cell::{XCell, XCellOwner};
	use crate::Variable;

	/// A helper type for all of the workarounds for nested variables
	#[must_use = "A `UseNested` type does nothing on its own, you need to call one of its method to actually perform the action."]
	pub struct UseNested<'a, C: XCell, F, CB, I> {
		pub(crate) var: &'a Variable<C>,
		pub(crate) owner: &'a mut C::Owner,
		pub(crate) get_inner: F,
		pub(crate) cb: CB,
		pub(crate) _marker: PhantomData<I>,
	}

	impl<'a, C: XCell, F, CB, R, I, CI: XCell> UseNested<'a, C, F, CB, I>
	where
		C::Owner: XCellOwner<Cell<I> = CI>,
		CB: FnOnce(&mut C::Owner, &mut Variable<CI>) -> R,
	{
		/// The simplest strategy to use a nested variable is to get the inner value by value (owning)
		/// so that you can borrow it independently from the owner.
		///
		/// The way to get the inner value out of its borrow is left up to `make_value`.
		/// For specialized forms, see [`simple_cloning`](Self::simple_cloning), [`simple_taking`](Self::simple_taking) and [`simple_replacing`](Self::simple_replacing).
		pub fn simple<E>(
			self,
			make_value: impl FnOnce(&mut C::Owner, &Variable<C>) -> C::Value,
		) -> Result<R, E>
		where
			C::Value: Sized,
			F: FnOnce(&mut C::Value) -> Result<&mut Variable<CI>, E>,
		{
			let Self {
				var,
				owner,
				get_inner,
				cb,
				_marker,
			} = self;

			let mut value = make_value(owner, var);

			let inner = get_inner(&mut value)?;
			let res = cb(owner, inner);

			*var.value_mut(owner) = value;

			Ok(res)
		}

		/// A shorthand for [`simple`](Self::simple) where the way to get the inner value by value is just cloning it.
		pub fn simple_cloning<E>(self) -> Result<R, E>
		where
			C::Value: Clone,
			F: FnOnce(&mut C::Value) -> Result<&mut Variable<CI>, E>,
		{
			self.simple(|owner, var| var.value(owner).clone())
		}

		/// A shorthand for [`simple`](Self::simple) where the way to get the inner value by value is by [`std::mem::take`]ing it.
		pub fn simple_taking<E>(self) -> Result<R, E>
		where
			C::Value: Default,
			F: FnOnce(&mut C::Value) -> Result<&mut Variable<CI>, E>,
		{
			self.simple(|owner, var| std::mem::take(var.value_mut(owner)))
		}

		/// A shorthand for [`simple`](Self::simple) where the way to get the inner value by value is by [`std::mem::replace`]ing it.
		pub fn simple_replacing<E>(self, with: C::Value) -> Result<R, E>
		where
			C::Value: Sized,
			F: FnOnce(&mut C::Value) -> Result<&mut Variable<CI>, E>,
		{
			self.simple(|owner, var| std::mem::replace(var.value_mut(owner), with))
		}

		/// A strategy where the inner variable is cloned before use, which is just an [`Rc::clone`], so it is very cheap.
		///
		/// The drawback of this is that changes to where that variable points don't apply to the original inner variable.
		/// This means that the original inner variable needs to be overwritten at the end of this method, and hence [`self.get_inner`](Self#structfield.get_inner) needs to be called twice.
		pub fn get_inner_twice<E>(self) -> Result<R, E>
		where
			F: FnMut(&mut C::Value) -> Result<&mut Variable<CI>, E>,
		{
			let Self {
				var,
				owner,
				mut get_inner,
				cb,
				_marker,
			} = self;

			let inner = get_inner(var.value_mut(owner))?;
			// note that this is a simple `Rc::clone` (no expensive computation)
			let mut inner2 = inner.clone();

			let res = cb(owner, &mut inner2);

			let inner3 = get_inner(var.value_mut(owner))?;
			*inner3 = inner2;

			Ok(res)
		}

		/// A strategy that uses double indirection.
		///
		/// In essence, it is similar to [`get_inner_twice`](Self::get_inner_twice), but instead of cloning the inner value,
		/// the inner value is already an `Rc<XCell<Variable<XCell<Foo>>>>` so that it can just be cloned,
		/// and all changes, including changes to where the variable points, are shared between the clone and the original.
		pub fn double_indirection<'b, D: XCell<Value = Variable<CI>>, E>(self) -> Result<R, E>
		where
			D::Owner: 'b,
			F: FnOnce(&mut C::Value) -> Result<(&'b mut D::Owner, Rc<D>), E>,
		{
			let Self {
				var,
				owner,
				get_inner,
				cb,
				_marker,
			} = self;

			let (wrapped_inner_owner, wrapped_inner) = get_inner(var.value_mut(owner))?;

			let inner = wrapped_inner_owner.rw(&*wrapped_inner);

			// note: since `inner` still points to inside `var`, no further cleanup is needed.
			let res = cb(owner, inner);

			Ok(res)
		}
	}
}
