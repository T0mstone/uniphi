/// An abstraction for the cell types from [`qcell`]
#[allow(missing_docs)]
pub trait XCell {
	type Owner: XCellOwner<Cell<Self::Value> = Self>;
	type Value: ?Sized;

	fn into_inner(self) -> Self::Value
	where
		Self::Value: Sized;
	fn get_mut(&mut self) -> &mut Self::Value;
}

/// An abstraction for the owner types from [`qcell`]
#[allow(missing_docs)]
pub trait XCellOwner {
	type Cell<T: ?Sized>: XCell<Owner = Self, Value = T> + ?Sized;

	fn cell<T>(&self, value: T) -> Self::Cell<T>;

	fn ro<'a, T: ?Sized>(&'a self, c: &'a Self::Cell<T>) -> &'a T;
	fn rw<'a, T: ?Sized>(&'a mut self, c: &'a Self::Cell<T>) -> &'a mut T;

	fn rw2<'a, T: ?Sized, U: ?Sized>(
		&'a mut self,
		c1: &'a Self::Cell<T>,
		c2: &'a Self::Cell<U>,
	) -> (&'a mut T, &'a mut U);
	fn rw3<'a, T: ?Sized, U: ?Sized, V: ?Sized>(
		&'a mut self,
		c1: &'a Self::Cell<T>,
		c2: &'a Self::Cell<U>,
		c3: &'a Self::Cell<V>,
	) -> (&'a mut T, &'a mut U, &'a mut V);
}

/// An abstraction for the types from [`qcell`] that are `repr(transparent)`
#[allow(missing_docs)]
pub trait TransparentXCell: XCell {
	fn ptr_to_inner(ptr: *const Self) -> *const Self::Value;
	fn ptr_from_inner(ptr: *const Self::Value) -> *const Self;
}

/// An abstraction for the types from [`qcell`] that can only ever have one owner per type (and possibly per thread),
/// which means that their values can be created without needing to reference an owner.
#[allow(missing_docs)]
pub trait UniquelyOwnedXCell: XCell {
	fn new(value: Self::Value) -> Self
	where
		Self::Value: Sized;
}

macro_rules! opt {
    (true => $($t:tt)*) => {
		$($t)*
	};
	(false => $($t:tt)*) => {};
	($t:tt => $($r:tt)*) => {
		compile_error!(concat!("opt!: expected `true` or `false`, got: ", stringify!($t)));
	}
}

macro_rules! delegate_xcell {
    (
		[$T:ident] $([$($igen:tt)*])?
		$XCell:ident [$($gen:tt)*]
		transparent = $transparent:tt
		unique = $unique:tt
		owner = $XCellOwner:ident $([$($ogen:tt)*])?
	) => {
		impl<$($($igen)*, )? $T: ?Sized> XCell for qcell::$XCell<$($gen)*> {
			type Owner = qcell::$XCellOwner $(<$($ogen)*>)?;
			type Value = $T;

			fn into_inner(self) -> Self::Value
			where
				Self::Value: Sized,
			{
				self.into_inner()
			}

			fn get_mut(&mut self) -> &mut Self::Value {
				self.get_mut()
			}
		}

		opt!(
			$transparent =>
			impl<$($($igen)* , )? $T: ?Sized> TransparentXCell for qcell::$XCell<$($gen)*> {
				fn ptr_to_inner(ptr: *const Self) -> *const Self::Value {
					ptr as _
				}
				fn ptr_from_inner(ptr: *const Self::Value) -> *const Self {
					ptr as _
				}
			}
		);

		opt!(
			$unique =>
			impl<$($($igen)* , )? $T: ?Sized> UniquelyOwnedXCell for qcell::$XCell<$($gen)*> {
				fn new(value: Self::Value) -> Self
				where
					Self::Value: Sized
				{
					Self::new(value)
				}
			}
		);

		impl<$($($igen)*, )?> XCellOwner for qcell::$XCellOwner $(<$($ogen)*>)? {
			type Cell<$T: ?Sized> = qcell::$XCell<$($gen)*>;

			fn cell<T>(&self, value: T) -> Self::Cell<T> {
				self.cell(value)
			}

			fn ro<'a, T: ?Sized>(&'a self, c: &'a Self::Cell<T>) -> &'a T {
				self.ro(c)
			}

			fn rw<'a, T: ?Sized>(&'a mut self, c: &'a Self::Cell<T>) -> &'a mut T {
				self.rw(c)
			}

			fn rw2<'a, T: ?Sized, U: ?Sized>(&'a mut self, c1: &'a Self::Cell<T>, c2: &'a Self::Cell<U>) -> (&'a mut T, &'a mut U) {
				self.rw2(c1, c2)
			}

			fn rw3<'a, T: ?Sized, U: ?Sized, V: ?Sized>(
				&'a mut self,
				c1: &'a Self::Cell<T>,
				c2: &'a Self::Cell<U>,
				c3: &'a Self::Cell<V>,
			) -> (&'a mut T, &'a mut U, &'a mut V) {
				self.rw3(c1, c2, c3)
			}
		}
	};
}

delegate_xcell!(
	[T] QCell[T]
	transparent = false
	unique = false
	owner = QCellOwner
);
delegate_xcell!(
	[T] [Q: 'static] TCell[Q, T]
	transparent = true
	unique = true
	owner = TCellOwner[Q]
);
delegate_xcell!(
	[T] [Q: 'static] TLCell[Q, T]
	transparent = true
	unique = true
	owner = TLCellOwner[Q]
);
delegate_xcell!(
	[T] ['id] LCell['id, T]
	transparent = true
	unique = true
	owner = LCellOwner['id]
);
